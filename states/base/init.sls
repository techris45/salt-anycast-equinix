base_packages:
  pkg.installed:
  - pkgs:
    - tcpdump
    - lldpd
    - jq
    - moreutils
