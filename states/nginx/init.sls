nginx:
  pkg.installed

/etc/nginx/nginx.conf:
  file.managed:
    - source: salt://nginx/files/nginx.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja

/etc/nginx/sites-available/default:
  file.managed:
    - source: salt://nginx/files/default.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja

nginx.service:
  service.running:
    - watch:
      - file: /etc/nginx/nginx.conf
      - file: /etc/nginx/sites-available/default

/var/www/html/index.nginx-debian.html:
  file.absent

/var/www/html/index.html:
  file.managed:
    - source: salt://nginx/files/index.html.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
