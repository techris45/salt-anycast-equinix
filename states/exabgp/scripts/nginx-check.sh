#!/bin/bash

while true; do
  curl -s localhost:80 > /dev/null;
  if [[ $? != 0 ]]; then
          {%- for ip in grains['anycast']['ipv4'] %}
          echo "withdraw route {{ ip['address'] }} next-hop {{ grains['bgp']['localip'] }}\n"
          {%- endfor %}
  else
          {%- for ip in grains['anycast']['ipv4'] %}
          echo "announce route {{ ip['address'] }} next-hop {{ grains['bgp']['localip'] }}\n"
          {%- endfor %}
  fi
  sleep 5
done
