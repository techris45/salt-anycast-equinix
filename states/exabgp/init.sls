exabgp:
  pip.installed

/etc/exabgp:
  file.directory:
    - user: root
    - group: root
    - mode: 755
    - makedirs: True

/etc/exabgp/scripts:
  file.directory:
    - user: root
    - group: root
    - mode: 755
    - makedirs: True

{% if not salt['file.is_fifo']('/var/run/exabgp.in') %}
exabgp_in_fifo:
  cmd.run:
    - name: mkfifo /var/run/exabgp.in
{% endif %}

{% if not salt['file.is_fifo']('/var/run/exabgp.out') %}
exabgp_out_fifo:
  cmd.run:
    - name: mkfifo /var/run/exabgp.out
{% endif %}

/etc/systemd/system/exabgp.service:
  file.managed:
    - source: salt://exabgp/files/exabgp.service.j2
    - user: root
    - group: root
    - mode: 0644
    - template: jinja

/etc/exabgp/exabgp.conf:
  file.managed:
    - source: salt://exabgp/files/exabgp.conf.j2
    - user: root
    - group: root
    - mode: 0644
    - template: jinja

exabgp_scripts:
  file.recurse:
    - name: /etc/exabgp/scripts
    - source: salt://exabgp/scripts
    - user: root
    - group: root
    - file_mode: 0755
    - template: jinja

exabgp_systemd_reload:
  cmd.run:
    - name: systemctl daemon-reload
    - onchanges:
      - file: /etc/systemd/system/exabgp.service

exabgp.service:
  service.running:
  - enable: True
  - reload: True
  - restart: True
  - watch:
      - file: /etc/exabgp/exabgp.conf
      - file: /etc/systemd/system/exabgp.service
      - file: exabgp_scripts
