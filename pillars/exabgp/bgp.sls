exabgp:
  asn: 65000
  check_script:
    name: "nginx-check.sh"
    encoder: text
  peers:
    - ip: 169.254.255.1 
      asn: 65530 
    - ip: 169.254.255.2 
      asn: 65530 
